const Bicicleta = require('../../models/bicicleta')

exports.bicicleta_list = function(req, res) {
  res.status(200).json({
    bicicletas: Bicicleta.allBicis
  })
}

exports.bicicleta_create = function(req, res) {
  const bici = new Bicicleta({
    code: req.body.code, 
    color: req.body.color, 
    modelo: req.body.modelo,
    ubicacion: [ req.body.lat, req.body.lng ]
  })

  Bicicleta.add(bici)

  res.status(200).json({
    bicicleta: bici
  })
}

exports.bicicleta_update = function(req, res) {
  const bici = {
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng]
  }

  const actualizarBici = Bicicleta.findOneAndUpdate({ code: req.body.code }, bici, () => {
    res.status(200).json({
      bicicleta: bici
    })
  })
}

exports.bicicleta_delete = function(req, res) {
  Bicicleta.removeByCode(req.body.code, (err, success) => {
    console.log(req.body.code)
    res.status(204).send()
  })
}