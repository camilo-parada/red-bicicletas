const mongoose = require('mongoose')
const Bicicleta = require('../../models/bicicleta')
const request = require('request')
const server = require('../../bin/www')

const base_url = 'http://localhost:3000/api/bicicletas'


describe('Bicicleta API', () => {
  beforeAll((done) => { 
    mongoose.connection.close(done) 
  })

  beforeEach((done) => {
    const mongoDB = 'mongodb://localhost/testdb'
    mongoose.connect(mongoDB, { useNewUrlParser: true })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'MongoDB connection error: '))
    db.once('open', () => {
      console.log('We are connected to database!')
      done()
    })
  })

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.error(err)
      // mongoose.disconnect(err)
      done()
    })
  })

  describe('GET BICICLETAS /', () => {
    it('Status 200', (done) => {
      request.get(base_url, (error, response, body) => {
        const result = JSON.parse(body)
        expect(response.statusCode).toBe(200)
        expect(result.bicicletas.length).toBe(0)
        done()
      })
    })
  })

  describe('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
      const headers = { 'content-type' : 'application/json' }
      const aBici = '{ "code": 1, "color": "Rojo", "modelo": "Montaña", "lat": -34, "lng": -54 }'
      request.post({
        headers: headers,
        url: base_url + '/create',
        body: aBici
      }, (error, response, body) => {
        expect(response.statusCode).toBe(200)
        const bici = JSON.parse(body).bicicleta
        console.log(bici)
        expect(bici.color).toBe('Rojo')
        expect(bici.ubicacion[0]).toBe(-34)
        expect(bici.ubicacion[1]).toBe(-54)
        done()
      })
    })
  })

  describe('POST BICICLETAS /delete', () => {
    it('Status 204', (done) => {
      const headers = { 'content-type': 'application/json' }
      const bici = new Bicicleta({ code: 1, color: "Rojo", modelo: "Montaña", lat: -34, lng: -54 })
      const borrarBici = { "code": bici.code }
      Bicicleta.add(bici)

      expect(Bicicleta.allBicis.length).toBe(1)
      
      request.delete({
        headers: headers,
        url: base_url + '/delete',
        body: JSON.stringify(borrarBici)
      }, (error, response, body) => {
        expect(response.statusCode).toBe(204)
        Bicicleta.allBicis((err, bicis) => {
          expect(bicis.length).toBe(0)
        })
        done()
      })
    })
  })

  describe('POST BICICLETAS /update', () => {
    it('Status 200', (done) => {
      const aBici = new Bicicleta({ code: 1, color: "Rojo", modelo: "Ruta", lat: -34, lng: -54 })

      Bicicleta.add(aBici, () => {
        const headers = { 'content-type': 'application/json' }
        const actualizarBici = { code: aBici.code, color: "Blanca", modelo: "Montaña", lat: -33, lng: -55 }
        request.post({
          headers: headers,
          url: base_url + '/update',
          body: JSON.stringify(actualizarBici)
        }, (error, response, body) => {
          expect(response.statusCode).toBe(200)
          const findBici = Bicicleta.findByCode(1, (err, bici) => {
            expect(bici.code).toBe(1)
            expect(bici.color).toBe(actualizarBici.color)
            expect(bici.modelo).toBe(actualizarBici.modelo)
            expect(bici.ubicacion[0]).toBe(actualizarBici.lat)
            expect(bici.ubicacion[1]).toBe(actualizarBici.lng)
          })
        })

        done()
      })
    })
  })

})