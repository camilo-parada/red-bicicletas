const mongoose = require('mongoose')
const Bicicleta = require('../../models/bicicleta')

describe('', () => {
  beforeEach((done) => {
    const mongoDB = 'mongodb://localhost/testdb'
    mongoose.connect(mongoDB, { useNewUrlParser: true })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'MongoDB connection error: '))
    db.once('open', () => {
      console.log('We are connected to database!')
      done()
    })
  })

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.error(err)
      mongoose.disconnect(err)
      done()
    })
  })

  describe('Bicicleta.createInstance', ()=> {
    it('crea una instancia de Bicicleta', () => {
      const bici = Bicicleta.createInstance(1, 'Negra', 'Montaña', [51.50555555, -0.09999999])

      expect(bici.code).toBe(1)
      expect(bici.color).toBe('Negra')
      expect(bici.modelo).toBe('Montaña')
      expect(bici.ubicacion[0]).toEqual(51.50555555)
      expect(bici.ubicacion[1]).toEqual(-0.09999999)
    })
  })

  describe('Bicicleta.allBicis', () => {
    it('comienza vacía', (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0)
        done()
      })
    })
  })

  describe('Bicicleta.add', ()=> {
    it('agrega solo una bici', (done) => {
      const bici = new Bicicleta({ code: 1, color: "Blanca", modelo: "Ruta" })
      Bicicleta.add(bici, (err, newBici) => {
        if (err) console.error(err)
        Bicicleta.allBicis((err, bicis) => {
          expect(bicis.length).toEqual(1)
          expect(bicis[0].code).toEqual(bici.code)
          done()
        })
      })
    })
  })

  describe('Bicicleta.findByCode', ()=> {
    it('debe devolver la bici con id 1', (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0)

        const bici1 = new Bicicleta({ code: 1, color: "Verde", modelo: "Ruta" })
        Bicicleta.add(bici1, (err, newBici) => {
          if (err) console.error(err)

          const bici2 = new Bicicleta({ code: 2, color: "Roja", modelo: "Urbana" })
          Bicicleta.add(bici2, (err, newBici) => {
            if (err) console.error(err)
            Bicicleta.findByCode(1, (error, targetBici) => {
              expect(targetBici.code).toBe(bici1.code)
              expect(targetBici.color).toBe(bici1.color)
              expect(targetBici.modelo).toBe(bici1.modelo)

              done()
            })
          })

        })
      })

      
      
    })
  })

})







// beforeEach(() => {
//   Bicicleta.allBicis = []
// })

// describe('Bicicleta.allBicis', () => {
//   it('comienza vacío', () => {
//     expect(Bicicleta.allBicis.length).toBe(0)
//   })
// })

// describe('Bicicleta.add', ()=> {
//   it('agrgeamos una', () => {
//     expect(Bicicleta.allBicis.length).toBe(0)

//     const bici = new Bicicleta(1, 'Negra', 'Montaña', [51.50555555, -0.09999999])
//     Bicicleta.add(bici)

//     expect(Bicicleta.allBicis.length).toBe(1)
//     expect(Bicicleta.allBicis[0]).toBe(bici)
//   })
// })

// describe('Bicicleta.findById', () => {
//   it('debe devolver la bici con id 1', () => {
//     expect(Bicicleta.allBicis.length).toBe(0)

//     const bici1 = new Bicicleta(1, 'Negra', 'Montaña', [51.50555555, -0.09999999])
//     const bici2 = new Bicicleta(2, 'Blanca', 'Ruta', [51.52777777, -0.097777777])

//     Bicicleta.add(bici1)
//     Bicicleta.add(bici2)

//     const targetBici = Bicicleta.findById(1)
//     expect(Bicicleta.allBicis.length).toBe(2)
//     expect(targetBici.id).toBe(1)
//     expect(targetBici.color).toBe(bici1.color)
//     expect(targetBici.modelo).toBe(bici1.modelo)
//   })
// })

// describe('Bicicleta.removeById', () => {
//   it('debe eliminar la bici con id 1', () => {
//     expect(Bicicleta.allBicis.length).toBe(0)

//     const bici1 = new Bicicleta(1, 'Negra', 'Montaña', [51.50555555, -0.09999999])
//     const bici2 = new Bicicleta(2, 'Blanca', 'Ruta', [51.52777777, -0.097777777])

//     Bicicleta.add(bici1)
//     Bicicleta.add(bici2)

//     Bicicleta.removeById(1)
//     expect(Bicicleta.allBicis.length).toBe(1)
//   })
// })