const mongoose = require('mongoose')
const Bicicleta = require('../../models/bicicleta')
const Usuario = require('../../models/usuario')
const Reserva = require('../../models/reserva')
const { find } = require('../../models/bicicleta')

describe('', () => {
  beforeEach((done) => {
    const mongoDB = 'mongodb://localhost/testdb'
    mongoose.connect(mongoDB, { useNewUrlParser: true })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'MongoDB connection error: '))
    db.once('open', () => {
      console.log('We are connected to database!')
      done()
    })
  })

  afterEach((done) => {
    Reserva.deleteMany({}, (err, success) => {
      if (err) console.error(err)
      Usuario.deleteMany({}, (err, success) => {
        if (err) console.error(err)
        Bicicleta.deleteMany({}, (err, success) => {
          if (err) console.error(err)
          mongoose.disconnect(err)
          done()
        })
      })
    })
  })

  describe('Cuando un Usuario reserva una bici', () => {
    it('debe existir la reserva', (done) => {
      const usuario = new Usuario({ nombre: 'Camilo' })
      usuario.save()
      const bicicleta = new Bicicleta({ "code": 1, "color": "Rojo", "modelo": "Montaña" })
      bicicleta.save()

      const hoy = new Date()
      const manana = new Date()
      manana.setDate(hoy.getDate()+1)
      usuario.reservar(bicicleta.id, hoy, manana, (err, reserva) => {
        Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
          console.log(reservas[0])
          expect(reservas.length).toBe(1)
          expect(reservas[0].diasDeReserva()).toBe(2)
          expect(reservas[0].bicicleta.code).toBe(1)
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre)
          done()
        })
      })

    })
  }) 

})