require('newrelic')
require('dotenv').config()

const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const passport = require('./config/passport')
const session = require('express-session')
const MongoDBStore = require('connect-mongodb-session')(session)
const jwt = require('jsonwebtoken')

const indexRouter = require('./routes/index')
const usuariosRouter = require('./routes/usuarios')
const tokenRouter = require('./routes/token')
const bicicletasRouter = require('./routes/bicicletas')
const bicicletasAPIRouter = require('./routes/api/bicicletas')
const usuariosAPIRouter = require('./routes/api/usuarios')
const authAPIRouter = require('./routes/api/auth')

const Usuario = require('./models/usuario')
const Token = require('./models/token')

let store
if (process.env.NODE_ENV === 'development') {
  store = new session.MemoryStore
} else {
  store = new MongoDBStore({
    uri: process.env.MONGODB_URI,
    collection: 'sessions'
  })
  store.on('error', (error) => {
    assert.ifError(error)
    assert.ok(false)
  })
}

const app = express()

app.set('secretKey', process.env.SECRET_KEY)

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: process.env.SESSION_SECRET
}))

const mongoose = require('mongoose')
const mongoDB = process.env.MONGODB_URI

mongoose.connect(mongoDB, { useNewUrlParser: true })
mongoose.Promise = global.Promise

const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error: '))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(passport.initialize())
app.use(passport.session())
app.use(express.static(path.join(__dirname, 'public')))

app.get('/login', function(req, res) {
  res.render('session/login')
})

app.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, usuario, info) => {
    if (err) return next(err)
    if (!usuario) return res.render('session/login', { info })
    req.logIn(usuario, (err) => {
      if (err) return next(err)
      return res.redirect('/')
    })
  })(req, res, next)
})

app.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

app.get('/forgotPassword', (req, res) => {
  res.render('session/forgotPassword')
})

app.post('/forgotPassword', (req, res, next) => {
  Usuario.findOne({ email: req.body.email }, (err, usuario) => {
    if(!usuario) return res.render('session/forgotPassword', { info: { message: 'No existe la clave' } })
    usuario.resetPassword((err) => {
      if(err) return next(err)
      console.log('session/forgotPasswordMessage')
    })
    res.render('session/forgotPasswordMessage')
  })
})

app.get('/resetPassword/:token', (req, res, next)=>{
  Token.findOne({ token: req.params.token }, (err, token) => {
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No existe una clave así' })

    Usuario.findById(token._userId, (err, usuario) => {
      if (!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado a este password' })
      res.render('session/resetPassword', { errors: {}, usuario: usuario })
    })
  })
})

app.post('/resetPassword', (req, res)=>{
  if(req.body.password !== req.body.confirm_password) {
    res.render('session/resetPassword', { errors: { confirm_password: { message: 'No coinciden las contraseñas' } } })
    return
  }
  Usuario.findOne({ email: req.body.email }, (err, usuario) => {
    usuario.password = req.body.password
    usuario.save(err=>{
      if(err){
        res.render('session/resetPassword', { errors: err.errors, usuario: new Usuario })
      } else {
        res.redirect('/login')
      }
    })
  })
})

// ROUTER
app.use('/', indexRouter)
app.use('/usuarios', usuariosRouter)
app.use('/token', tokenRouter)
app.use('/bicicletas', loggedIn, bicicletasRouter)

// API ROUTER
app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter)
app.use('/api/usuarios', usuariosAPIRouter)

app.use('/privacy_policy', (req, res) => {
  res.sendFile('public/privacy_policy.html')
})

app.use('/googlefc94e571eb761925', (req, res) => {
  res.sendFile('public/googlefc94e571eb761925.html')
})

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read' ,
    'profile',
    'email' ] }
))

app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/error'
}))

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

function loggedIn(req, res, next) {
  if (req.user) {
    next()
  } else {
    console.log('user sin loguearse')
    res.redirect('/login')
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), (err, decoded) => {
    if (err) {
      res.json({ status: "error", message: err.message, data: null })
    } else {
      req.body.userId = decoded.id
      console.log('JWT verify: ', decoded)

      next()
    }
  })
}


module.exports = app
