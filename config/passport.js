const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const Usuario = require('../models/usuario')
const GoogleStrategy = require("passport-google-oauth20").Strategy
const FacebookTokenStrategy = require('passport-facebook-token')

passport.use(new LocalStrategy({ usernameField: "email" }, (email, password, done) => {
    console.log('receive', email, password);
    Usuario.findOne({ email: email }, (err, usuario) => {
      if (err) return done(err)
      if (!usuario) return done(null, false, { message: 'Email no existente o incorrecto' })
      if (!usuario.validPassword(password)) return done(null, false, { message: 'Password incorrecto' })

      return done(null, usuario)
    })
  }
))

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.HOST + "/auth/google/callback"
}, (accessToken, refreshToken, profile, cb) => {
    console.log(profile)
    
    Usuario.findOneOrCreateByGoogle(profile, (err, user) => {
      return cb(err, user);
    })
  }
))

passport.use(new FacebookTokenStrategy({
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET
  }, (accessToken, refreshToken, profile, done) => {
    Usuario.findOneOrCreateByFacebook(profile, (err, user) => {
      if (err) {
        console.log('err' + err)
        return done(err, user)
      }
      done(null, user)
  })
  }
))

passport.serializeUser((user, cb) => {
  cb(null, user.id)
})

passport.deserializeUser((id, cb) => {
  Usuario.findById(id, (err, usuario) => {
    cb(err, usuario)
  })
})

module.exports = passport