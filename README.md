# Desarrollo del lado servidor: NodeJS, Express y MongoDB

Curso de Desarrollo del lado servidor: NodeJS, Express y MongoDB (Coursera)

El proyecto esta dividido por semanas, por lo que se crearon branches para cada una de estas. Dentro del último commit de cada semana se encuentran los objetivos a evaluar.

* * * 

## Semana 1 (branch: semana_1)

Creación del proyecto, integración del template, el mapa, el MVC, el CRUD de elementos y el API

### Objetivos a evaluar: 

1. Un archivo README (sólo texto) en el repositorio de Bitbucket.

2. El mensaje de bienvenida a Express.
   
3. El proyecto vinculado con el repositorio de Bitbucket creado previamente.

4. El servidor que se ve correctamente, comparándolo con la versión original.

5. Un mapa centrado en una ciudad. marcadores indicando una ubicación.

6. Un script npm en el archivo package.json, debajo del “start”, que se llama “devstart” y que levanta el servidor utilizando npdemon.

7. Un par de bicicletas en la colección que conoce la bicicleta en memoria, con las ubicaciones cercanas al centro del mapa agregado.

8. Una colección del modelo de bicicleta.

9. Los endpoints de la API funcionando con Postman.

* * *

## Semana 2 (branch: semana_2)

Integración de test unitarios con Jasmine y MongoDB con Mongoose como ODM

### Objetivos a evaluar: 

1. Una carpeta con nombre “models” dentro de la carpeta spec.

2. Los tests aprobados

3. Tests de cada operación de la API de bicicleta.

4. Un archivo bicicleta_api_test.spec.js

5. Todos los tests aprobados al ejecutar el comando npm test. 

6. Las colecciones actuales Desde Mongo Compass, conectado a tu base local de Mongo.

7. Una base local mongo llamada “red_bicicletas” conectada usando mongoose.

8. El modelo funcional utilizando Postman.

9. Documentos generados en la base local con Mongo Compass.

10. tests del modelo Bicicleta que usan persistencia.

* * * 

## Semana 3 (branch: semana_3)

Integración de Passport, JWT para procesos de autenticación y autorización, además de envío de emails con NodeMailer

### Objetivos a evaluar: 

1. Los atributos, con las restricciones correspondientes: email, password, passwordResetToken, passwordResetTokenExpires, verificado.

2. El modelo de Token que debe tener una referencia al usuario asignado, el token propiamente dicho y la fecha de creación. 

3. Un email ejecutado en algún paso del proyecto.

4. El email de bienvenida con el link de verificación de cuenta.

5. Vistas de login, recupero de password y demás.

6. Las funciones de serializeUser y deserializeUser en el archivo “passport.js”.

7. Credenciales verdaderas e incorrectas.

8. Que se te niega el acceso al escribir directamente las url en el navegador; y si no estás logueado en el sistema, eres redirigido al login.

9. Utilizando Postman, te autenticas enviando credenciales correctas y obteniendo un token como respuesta.

10. El token válido después de autenticarte y acceder a los recursos de bicicletas utilizando Postman. Y un mensaje de error sin haberte autenticado.

* * * 

## Semana 4 (branch: semana_4)

Integración de Autenticaciones vía OAuth (Google y Facebook), además de NewRelic para monitorear el servidor

### Objetivos a evaluar: 

1. La app publicada a través del servicio de heroku.

2. El usuario creado en la base de mongo, utilizando el visor web o mongo compa

3. Las variables NODE_ENV=”production” y MONGO_URI=”[dirección de conexión con mongo atlas]”

4. El mongo local utilizado correctamente.

5. Que haciendo un deploy a Heroku, la aplicación productiva utiliza mongo atlas.

6. Que el ambiente local sigue enviando los mails por ethereal.

7. Que los emails se envían por Sendgrid (puedes crear un usuario con un email tuyo ya que esa acción envía un email de verificación).

8. El método “findOneOrCreateByGoogle”  en el modelo de usuario.

9. La validación del token de Facebook.

10. La librería “newrelic”  en  app.js el require(‘newrelic’).